import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		Random rand = new Random();
		int num = rand.nextInt(100);
		
		int count = 0;
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.println("Can you guess it: ");
		
		
		while (true) {
			int guess = scanner.nextInt();
			
			if (guess == - 1) {
				System.out.println("Sorry, the number was " + num);
				break;
			}
			
			if (guess < num) {
				System.out.print("Sorry!\n" 
					    + "Mine ise greater than your guess.\n"
						+ "Type -1 to quit or guess another:\n");
				
			} else if  (guess > num) {
				
				System.out.print("Sorry!\n"
						+ "Mine is less than your guess.\n"
						+ "Type -1 to quit or guess another:\n");
				
			} else {
				System.out.println("Congratulations! You won after " +  count + " attempts!");
				break;
			}
			
			count++;
			
		}
	}

}