public class FindPrimes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub    
        
	    int val = Integer.valueOf(args[0]);
    
	    for (int i = 1; i <= val; i++) {
	    	
	    	int count = 0;
	    	
	    	for (int num = i; num >= 1; num--) {
	    		
	    		if (i % num == 0) {
	    			count++;
	    		}
	    		
	    	}
	    	if (count == 2) {
	    		System.out.print(i + " ");
	    	}	
	    }	       
	}
}